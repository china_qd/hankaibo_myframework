<!-- 公共CSS资源 -->
<link rel="stylesheet" href="${ctx}/resources/css/bootstrap-3.3.4/dist/css/bootstrap.min.css" />
<link rel="stylesheet" href="${ctx}/resources/js/jquery-ui-1.11.4.custom/jquery-ui.theme.min.css" />
<!-- 公共JS资源 -->
<script src="${ctx}/resources/js/jquery-1.11.0.min.js"></script>
<script src="${ctx}/resources/js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="${ctx}/resources/css/bootstrap-3.3.4/dist/js/bootstrap.min.js"></script>