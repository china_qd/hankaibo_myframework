/**
 * Copyright © 2015.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package cn.cpbpbc.dao;

import cn.cpbpbc.po.ReportData;
import cn.mypandora.orm.dao.IBaseEntityDao;

/**
 * cpbpbc报表DAO.
 * <p>User: hankaibo
 * <p>Date: 2015/7/19
 * <p>Version: 1.0
 */
public interface ReportDao extends IBaseEntityDao<ReportData> {
}
