/**
 * Copyright © 2015.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package cn.cpbpbc.po;

import cn.mypandora.orm.model.BaseEntity;

/**
 * cpbpbc报表实体。
 * <p>User: hankaibo
 * <p>Date: 2015/7/19
 * <p>Version: 1.0
 */
public class ReportData extends BaseEntity {
    //数据拥有者
    private String dataOwner;
    //数据
    private Integer data1;
    private Integer data2;
    private Integer data3;
    private Integer data4;
    private Integer data5;
    private Integer data6;
    private Integer data7;
    private Integer data8;
    private Integer data9;
    private Integer data10;
    private Integer data11;
    private Integer data12;
    private Integer data13;
    private Integer data14;
    private Integer data15;
    private Integer data16;
    private Integer data17;
    private Integer data18;
    private Integer data19;
    private Integer data20;
    private Integer data21;
    private Integer data22;
    private Integer data23;
    private Integer data24;
    private Integer data25;
    private Integer data26;
    private Integer data27;
    private Integer data28;
    private Integer data29;
    private Integer data30;

    public String getDataOwner() {
        return dataOwner;
    }

    public void setDataOwner(String dataOwner) {
        this.dataOwner = dataOwner;
    }

    public Integer getData1() {
        return data1;
    }

    public void setData1(Integer data1) {
        this.data1 = data1;
    }

    public Integer getData2() {
        return data2;
    }

    public void setData2(Integer data2) {
        this.data2 = data2;
    }

    public Integer getData3() {
        return data3;
    }

    public void setData3(Integer data3) {
        this.data3 = data3;
    }

    public Integer getData4() {
        return data4;
    }

    public void setData4(Integer data4) {
        this.data4 = data4;
    }

    public Integer getData5() {
        return data5;
    }

    public void setData5(Integer data5) {
        this.data5 = data5;
    }

    public Integer getData6() {
        return data6;
    }

    public void setData6(Integer data6) {
        this.data6 = data6;
    }

    public Integer getData7() {
        return data7;
    }

    public void setData7(Integer data7) {
        this.data7 = data7;
    }

    public Integer getData8() {
        return data8;
    }

    public void setData8(Integer data8) {
        this.data8 = data8;
    }

    public Integer getData9() {
        return data9;
    }

    public void setData9(Integer data9) {
        this.data9 = data9;
    }

    public Integer getData10() {
        return data10;
    }

    public void setData10(Integer data10) {
        this.data10 = data10;
    }

    public Integer getData11() {
        return data11;
    }

    public void setData11(Integer data11) {
        this.data11 = data11;
    }

    public Integer getData12() {
        return data12;
    }

    public void setData12(Integer data12) {
        this.data12 = data12;
    }

    public Integer getData13() {
        return data13;
    }

    public void setData13(Integer data13) {
        this.data13 = data13;
    }

    public Integer getData14() {
        return data14;
    }

    public void setData14(Integer data14) {
        this.data14 = data14;
    }

    public Integer getData15() {
        return data15;
    }

    public void setData15(Integer data15) {
        this.data15 = data15;
    }

    public Integer getData16() {
        return data16;
    }

    public void setData16(Integer data16) {
        this.data16 = data16;
    }

    public Integer getData17() {
        return data17;
    }

    public void setData17(Integer data17) {
        this.data17 = data17;
    }

    public Integer getData18() {
        return data18;
    }

    public void setData18(Integer data18) {
        this.data18 = data18;
    }

    public Integer getData19() {
        return data19;
    }

    public void setData19(Integer data19) {
        this.data19 = data19;
    }

    public Integer getData20() {
        return data20;
    }

    public void setData20(Integer data20) {
        this.data20 = data20;
    }

    public Integer getData21() {
        return data21;
    }

    public void setData21(Integer data21) {
        this.data21 = data21;
    }

    public Integer getData22() {
        return data22;
    }

    public void setData22(Integer data22) {
        this.data22 = data22;
    }

    public Integer getData23() {
        return data23;
    }

    public void setData23(Integer data23) {
        this.data23 = data23;
    }

    public Integer getData24() {
        return data24;
    }

    public void setData24(Integer data24) {
        this.data24 = data24;
    }

    public Integer getData25() {
        return data25;
    }

    public void setData25(Integer data25) {
        this.data25 = data25;
    }

    public Integer getData26() {
        return data26;
    }

    public void setData26(Integer data26) {
        this.data26 = data26;
    }

    public Integer getData27() {
        return data27;
    }

    public void setData27(Integer data27) {
        this.data27 = data27;
    }

    public Integer getData28() {
        return data28;
    }

    public void setData28(Integer data28) {
        this.data28 = data28;
    }

    public Integer getData29() {
        return data29;
    }

    public void setData29(Integer data29) {
        this.data29 = data29;
    }

    public Integer getData30() {
        return data30;
    }

    public void setData30(Integer data30) {
        this.data30 = data30;
    }
}
